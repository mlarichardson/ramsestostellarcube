import numpy as np
import os
#import pymses
#from pymses.utils.regions import Cube
#from pymses.utils import constants as C
#from pymses.filters import RegionFilter, PointFunctionFilter
#from pymses.analysis.visualization import *
import matplotlib.pyplot as plt
import pynbody
import pynbody.filt as filt
import astropy.io.ascii as ascii
import astropy.io.fits as fits
import astropy.cosmology as cosmo
#from pyfriedman import friedman
import scipy.interpolate as interpolate
import pdb

#plt.close('all')


# Location of the file to read in.
input_path = './'

# output file number
out_no = 149

# Name of the file to be produced by this program
output_file = './ramses_parts_SF2FB2.fits'

# specify the centre coords of the galaxy in grid units, and its size (RADIUS in kpc; represents 3 * Reff)
# These are the coordinates for the SF2FB2.
cen = [0.70673126, 0.33313322, 0.33985671]
galSize = "5 kpc"

# Specify stellar age lookup file:
lkp = './ramses_age_lookup.txt'



# -------------------------------------------------------------------------------- #
# Below here you shouldn't need to change

# Load data
s = pynbody.load(input_path+'/output_00{0}'.format(out_no)) 

# print out some basic info:
info = s.properties

# get cosmo parameters from the info.
omega_m = info['omegaM0']
omega_l = info['omegaL0']
omega_k = 1.0 - omega_m - omega_l
H0 = info['h']*100.
a = info['a']

# info on gas, stars, dm particles are in s.stars (s.s), s.dark (s.d) and s.gas (s.g)

# Recenter, convert units
s['pos'] = s['pos'] - cen
s.physical_units()
galSub = s[filt.Sphere(galSize)]


# Produce an ASCII file for the star particles in our galaxy region, with the properties that are relevant for our work
# The keys required are: pos (x,y,z), velocity (x,y,z), mass, temp, metallicity, age
starArr = np.zeros((len(galSub.s)), dtype=[('posx', 'f8'), ('posy', 'f8'), ('posz', 'f8'), ('velx', 'f8'), ('vely', 'f8'), ('velz', 'f8'), 
                ('mass', 'f8'), ('epoch', 'f8'), ('age3', 'f8'), ('metal', 'f8')])

# now calculate the actual ages of the particles from the epoch field, which is conformal time

# load in the lookup table
agetab = ascii.read(lkp, delimiter=' ')
agetab['conformal_age'] = agetab['conformal_age'][-1::-1]
agetab['age_Gyr'] = agetab['age_Gyr'][-1::-1]

iftn = interpolate.interp1d(agetab['conformal_age'], agetab['age_Gyr'], bounds_error=False)
partAge = iftn(galSub.s['age'])

starArr['posx'] = galSub.s['pos'][:,0]
starArr['posy'] = galSub.s['pos'][:,1]
starArr['posz'] = galSub.s['pos'][:,2]
starArr['velx'] = galSub.s['vel'][:,0]
starArr['vely'] = galSub.s['vel'][:,1]
starArr['velz'] = galSub.s['vel'][:,2]
starArr['mass'] = galSub.s['mass']
starArr['epoch'] = galSub.s['age']
starArr['age3'] = partAge
starArr['metal'] = galSub.s['metal']
#starArr['temp'] = galSub.s['temp']

# Write a FITS table

col1 = fits.Column(name='posx', format='E', unit='kpc')
col2 = fits.Column(name='posy', format='E', unit='kpc')
col3 = fits.Column(name='posz', format='E', unit='kpc')
col4 = fits.Column(name='velx', format='E', unit='km s-1')
col5 = fits.Column(name='vely', format='E', unit='km s-1')
col6 = fits.Column(name='velz', format='E', unit='km s-1')
col7 = fits.Column(name='mass', format='E', unit='msol')
col8 = fits.Column(name='epoch', format='E', unit='conformal')
col9 = fits.Column(name='age3', format='E', unit='Gyr')
col10 = fits.Column(name='metal', format='E')

coldefs = fits.ColDefs([col1, col2, col3, col4, col5,col6,col7,col8,col9,col10])
tbhdu = fits.BinTableHDU.from_columns(coldefs)
tbhdu.data = starArr

# This has created the right keywords in the header. Now add in extra keywords with cosmo parameters. 
h = tbhdu.header
h.comments['TTYPE1'] = 'KPC'
h.comments['TTYPE2'] = 'KPC'
h.comments['TTYPE3'] = 'KPC'
h.comments['TTYPE4'] = 'KM/S'
h.comments['TTYPE5'] = 'KM/S'
h.comments['TTYPE6'] = 'KM/S'
h.comments['TTYPE7'] = 'MSOL'
h.comments['TTYPE8'] = 'FORMATION TIME IN COSMO UNITS'
h.comments['TTYPE9'] = 'AGE AT Z=3, GYR'
h.comments['TTYPE10'] = 'METALLICITY'

h.append(('GALX', cen[0], 'X COORD IN FULL SIM'))
h.append(('GALY', cen[1], 'Y COORD IN FULL SIM'))
h.append(('GALZ', cen[2], 'Z COORD IN FULL SIM'))
h.append(('Z', 1./a - 1., 'REDSHIFT'))
h.append(('OMEGAM', omega_m))
h.append(('OMEGAL', omega_l))
h.append(('OMEGAK', omega_k))
h.append(('SCALEFAC', a))
h.append(('H0', H0))

tbhdu.writeto(output_file)
