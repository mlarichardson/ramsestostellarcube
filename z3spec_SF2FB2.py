'''This script will read in the star particles for the z=3
galaxy of interest and match them up with a spectrum class from
Vazdekis2015 models.

Author: Sarah Kendrew & Simon Zieleniewski & Mark Richardson

Last updated: 05-06-17

'''

import numpy as np
import scipy.stats as stats
import scipy.constants as sc
import scipy.interpolate as intp
import scipy.integrate as integrate
import scipy.signal as signal
import matplotlib.pyplot as plt
import astropy.io.ascii as ascii
import astropy.io.fits as fits
import astropy.units as u
from astropy.convolution import Gaussian1DKernel, convolve, convolve_fft
from astropy.cosmology import LambdaCDM
from ppxf_util import log_rebin
import spectools as st
#import BC03tools as bc
import V12tools as vt
import time
import pdb
import os
from os.path import expanduser

plt.close('all')

# change inmplemented 27.04.15:
# rebinning into log(lambda) space with flux conservation to adjust for star particles' line of sight velocity.

comp = os.uname()
if (comp[1] == 'asosxlap100'):
    print "--> This is your laptop"
    comp = 'laptop'
elif (comp[1] == 'harmonisim'):
    print "--> This is the HARMONISIM machine"
    comp = 'HARMONISIM'

#==================================================
def createCosmo(h):
    cosmology = LambdaCDM(H0=h['H0'], Om0=h['OMEGAM'], Ode0=h['OMEGAL'])
    return cosmology
#==================================================
def findStars(stars, sx, sy, spax_ind1, spax_ind2):
    # This function returns a boolean array identifying the stars in a given spaxel
    pick = ((sx == spax_ind1+1) & (sy == spax_ind2+1))
    #print 'found {0} stars' .format(np.size(s[pick]))
    return pick
#==================================================
def createHeader(h, cube, grid_spatial, grid_spectral):
    # function to create the proper header for the output Cube
    # arguments:
    # hin = basic PrimaryHDU header object
    # cube = the data cube
    # grid_spatial = the vector of spatial values (for spatial dimensions)
    # grid_spectral = the vector of wavelength values
    
    h['CTYPE1'] = 'RA'
    h['CTYPE2'] = 'DEC'
    h['CTYPE3'] = 'WAVELENGTH'
    h['CUNIT1'] = 'ARCSEC'
    h['CUNIT2'] = 'ARCSEC'
    h['CUNIT3'] = 'angstroms'
    h['CRVAL1'] = grid_spatial[0]
    h['CRVAL2'] = grid_spatial[-1]
    h['CRVAL3'] = grid_spectral[0]
    h['CDELT1'] = np.diff(grid_spatial)[0]
    h['CDELT2'] = np.diff(grid_spatial)[0]
    h['CDELT3'] = np.diff(grid_spectral)[0]
    h['CRPIX1'] = 1
    h['CRPIX2'] = 1
    h['CRPIX3'] = 1
    h['FUNITS'] = 'erg/s/cm2/A/arcsec2'
    h['SPECRES'] = 1.
    
    return h
#==================================================
def calcRestWave(rv, samp, harmoni=[0.4, 3.0], z=3.0):
    '''
    This function calculates the optimal common wavelength grid for the final cube. There are 2 steps:
    1. calculate the wavelengths (min, max) that get redshifted into the HARMONI wavelength range at redshift z
    2. calculate the common range based on the highest radial velocity values given the particle data
    
    Input:
    inpWave:    the BC03 spectra's wavelength grid
    harmoni:    a tuple of [minwave, maxwaeve] appropriate for Harmoni, in microns. Default is 400 nm to 3 micron
    z:          redshift, scalar (default = 3)
    rv:         a grid of radial velocities for the particles. must be same length as number of particles.
    
    '''
    
    # Calculate the min, max wavelength that will get redshifted into the HARMONI bands. convert to AA.
    lmin = (harmoni[0] / (1.+z)) * 1e4
    lmax = (harmoni[1] / (1.+z)) * 1e4
    
    # find the largest blueshift and apply to lmin (don't forget to convert rv to m/s!)
    maxblue = np.min(rv[rv<0.])
    print 'max blueshift = {0} km/s' .format(maxblue)
    lmin_corr = lmin * (1. + (maxblue*1e3/sc.c))
    lmin_corr = np.floor(lmin_corr)
    maxred = np.max(rv[rv>0.])
    print 'max redshift = {0} km/s' .format(maxred)
    lmax_corr = lmax * (1. + (maxred*1e3/sc.c))
    lmax_corr = np.ceil(lmax_corr)
    
    outWave = np.arange(lmin_corr, lmax_corr, samp, dtype=float)
    print 'output wave range covers {0} to {1} AA' .format(lmin_corr, lmax_corr)
    
    return outWave
#==================================================
def genSpaxSpec(stars, specs, lrange, Zvals, Zpos, agevals, vvals):
    #print '>>> Generating spaxel spectrum.......'
    #pdb.set_trace()
    # calculate the ages of the particles at z=3.2
    # TEST TEST SET ALL STAR PARTICLE AGES TO THE AGE OF THE OLDEST ONE
    # age32 = stars['age3'] - 0.1539574
    
    #age32 = (np.max(stars['age3']) - 0.1539574) * np.ones(np.size(stars))
    
    
    start = time.time()
    nstars = np.size(stars)
    rawWave = specs[0].lam
    
    #wrange = [np.min(xwave)-100., np.max(xwave)+100.]
    # set the wavelength limits but give a bit of extra range so the interplation doesn't crash.
    rawSpec = np.zeros((nstars, len(specs[0].lam)), dtype=float)
    rvSpec = np.zeros((nstars, len(specs[0].lam)), dtype=float)
    #iSpec = np.zeros((nstars, np.size(xwave)), dtype=float)
    
    # need to specify a multiplier to account for the face that the MIUSCAT spectra aren't in ascending metallicity order
    #Zmult = [4, 3, 2, 1, 0, 5, 6]
    
    if (nstars == 0):
        addSpec = iSpec
    
    elif (nstars == 1):
        # select the right spectrum
        Zind = np.argmin(np.abs(stars['metal']-Zvals))
        #ind = 33 * Zmult[Zind]
        ind = Zpos[Zind]
        ageind = np.argmin(np.abs(stars['age3']-agevals))        
        ind = ind + ageind
        rawSpec = specs[ind].flam[0] * stars['mass']
        
        # correct for radial velocity --> here Y 
        rvSpec = vshiftSpec(rawWave, rawSpec, stars['vely'])
        #iSpec = interpSpec(rvWave, rawSpec, xwave)
        
        
    else:
        # start by matching stars with model spectra
        Zind = np.digitize(stars['metal'], Zvals)
        #ageind = np.digitize(stars['age3'], agevals)
        ageind = np.digitize(stars['age3'], agevals)
        vind = np.digitize(stars['vely'], vvals)        
        
        #colours = ['r', 'b', 'g', 'm', 'k', 'y', 'c']
        
        
        # Match the particles with their spectra, scaled for particle mass
        #pdb.set_trace()
        for i in range(nstars):
            
            # have to reconstruct the index of the required spectrum from the Zind, ageind values
            #ind = Zmult[Zind[i]] * 33
            ind = Zpos[Zind[i]-1] # digitize now gives up bound for increasing
            # need to then increment ind with the age index for the star
            ind = ind + ageind[i]-1
            
            # Pull out the appropriate template spectrum
            rawSpec[i,:] = specs[ind].flam[0] * stars['mass'][i]
            
            # Do the line of sight velocity shifting
            rvSpec[i,:] = vshiftSpec(rawWave, rawSpec[i,:], stars['vely'][i])
            
        
            #rvlogWave[i,:] = vshiftSpec(logLam, stars['vely'][i])
            #iSpec[i,:] = interpSpec(rvlogWave[i,:], rawSpec[i,:], xlogWave)
            #iSpec[i,:] = interpSpec(rvlogWave[i,:], logSpec, xlogWave)
            
    
        
        # now we have the spectra, sampled onto the same regular grid, finely sampled, so we can sum them up!
    if (nstars ==1):
        addSpec = rvSpec
    else:
        addSpec = np.sum(rvSpec, axis=0)
    
    # now just cut down to the required range
    lcut = (rawWave >= lrange[0]) & (rawWave <= lrange[1])
    outSpec = addSpec[lcut]
    outWave = rawWave[lcut]
    

        
    end = time.time()
    #print 'nstars = {0}; time taken: {1}' .format(nstars, end-start)
        
    return outWave, outSpec
#==================================================
def vshiftSpec(inWave, inSpec, rv):
    '''
    Function that will apply a radial velocity shift to the input spectrum
    
    Input:
    inWave:     1D array of floats, input wavelength grid
    inSpec:     1D spectrum
    rv:         scalar, radial velocity (km/s)
    
    Output:
    rvWave:     1D array of floats, length same as inWave, RV-shifted wavelength grid (km/s)
    '''
    #print 'radial velocity to apply: {0}' .format(rv)
    
    
    # Rebin the spectrum onto log(lambda) scale so can apply a linear shift
    lamrange = [inWave[0], inWave[-1]]
    logspec, loglam, velscale = log_rebin(lamrange, inSpec, velscale=20., flux=False)
    
    # convert rv to m/s, same units as sc.c, and shift the log(wave) axis
    rv = rv*1e3
    loglam += rv/sc.c
    
    #pdb.set_trace()
    # now interpolate back onto the input linear grid. sort the edges out later. NOTE: THE NUMPY INTERPOLATION ROUTINE IS MUCH FASTER THAN THE SCIPY ONE!
    #fInt = intp.interp1d(np.exp(loglam), logspec, kind='linear', assume_sorted=True, bounds_error=False, fill_value=0.)
    outSpec = np.interp(inWave, np.exp(loglam), logspec) 
    
    #outSpec = fInt(inWave)


#    newWave = inWave*(1.+ (rv/sc.c))
    #print 'Delta wave to apply: {0} (AA)' .format(newWave[0] - inWave[0])
    #newSpec = inSpec
    
    return outSpec
#==================================================
def interpSpec(inpWave, inpSpec, newWave):
    #ncols = np.size(inpSpec, axis=0)
    if np.size(inpWave, axis=0) != np.size(inpSpec, axis=0):
        raise ValueError("input wavelengths and spectra arrays must have same dimensions")
    
    intspec = intp.interp1d(inpWave, inpSpec, kind='linear')
    outSpec = intspec(newWave)
    
    return outSpec
#==================================================
def smoothSpec(inpWave, inSpec, width=10.):
    # this function will convolve the spectrum with a 10 km/s (or other value as provided) kernel to smoothen it out
    # it calculates the width of 10 km/s in the middle of the wavelength range, constructs a gaussian kernel and convolves
    # returns the output spectrum, with the same length as the input
    # NOTE: the resolving power will not be the same over the full range
    
    # first check whether the specified width isn't actually smaller than the sampling
    lsamp = np.diff(inpWave)[0]
    delta_l = np.median(inpWave) * (width*1e3/sc.c)
    outSpec = inSpec
    
    
    return outSpec
#==================================================



start = time.time()

# Use Ryan's tools to create the grid of model spectra
specs = vt.loadV12ssps(sedpath='./SpecFiles/', verbose=False)

# Output file from pynramses:
f = './ramses_parts_SF2FB2.fits'

# If metal is missing from the other file. pynramses should be changed if this is the case. 
metal_fix = None

# Do you want to brighten the stars by a factor Brighten?
brighten_factor = None

# Set velocity range and resolution
vmin = -1100.
vmax =  1050.
dv   =   50.

# Set width of box in kpc (2*radius used in pynramses)
width_box = 10.

# Set lambda range in cube: 
### FOR THE H BAND CA H+K RANGE, USE THIS:
llims = [3800., 5500.]

### FOR THE K BAND MG B RANGE, USE THIS:
#llims = [5000., 5500.]

### For the supernova cube USE THIS:
#llims = [3625., 6125.]
#llims = [3600., 7200.]

# Name of final output cube to be generated:
output_cube = './ramses_SF2FB2_z3.0_vazdekis_CaHK_spax5.fits'



# ------------------------------------------------ #
# Nothing below should need to be changed

t1 = time.time()
print 'spectra loaded in {0} s' .format(t1-start)

hdu = fits.open(f)
s = hdu[1].data
hdr = hdu[1].header


# TEMPORARY FIX FOR ABSENT METALLICITIES: SET THEM ALL TO SOLAR
if metal_fix != None:
  s['metal'] = metal_fix

t2 = time.time()
print 'particles loaded in {0} s' .format(t2-t1)

# Rescale star masses to get same brightness. SF2FB2 = SF1FB1 / 6.84
if brighten_factor != None:
  s['mass'] = s['mass']*brighten_factor

# Extract the redshift from the header. Let's not call it Z to avoid confusion later with metallicity Z.
rshift = hdr['Z']
print 'z = {0}' .format(rshift)

# Create a custom LCDM cosmology for further computations from the FITS header
cosmo = createCosmo(hdr)

npart = np.size(s)

# Step 1: for each particle find the closest match
tmp = np.zeros(len(specs))

for i in range(len(specs)):
    tmp[i] = specs[i].Z

Zvals, Zpos = np.unique(tmp, return_index=True)

# Ditto for the ages. The age reference values are the same for all metallicity spectra, that keeps it simple.
nages = len(specs)/len(Zvals)
agevals = np.zeros(nages)

for i in range(nages):
    agevals[i] = specs[i].age[0]


# Now ditto for the velocities. Create a grid of velocities (along z axis),
# and map the particles onto that grid.
# The range of velocities is -1032 -> 956 km/s
vvals = np.arange(vmin, vmax, dv)

# Calculate the angular distance scaling from the cosmology.
# Standard returns a quantity in kpc/arcmin so need to convert to kpc/arcsec. 
# Finally calculate the angular size of a 10 kpc sized box
da = cosmo.kpc_proper_per_arcmin(rshift)
da = da.to(u.kpc/u.arcsec)
fov = (width_box*u.kpc)/da

spax_size = 5.*u.mas
nspax = np.ceil(fov.to(u.mas) / spax_size)
nspax = np.int(nspax.value)

# convert the x and y positions to angular positions on the sky
#  NOTE: using a Y projection: x --> z, y --> x
posx_ang = s['posz'] / da
posy_ang = s['posx'] / da

# produce a vector of spaxel egdes based on the number of
# spaxels required (nspax) and the spaxel scale (spax_size).
# offset by half a pixels so each spaxel bin is centred on the
# midpoint of each spaxel -- NO don't do this in this case right?

# the same vector can be used in x and y as the dimesions are the same.
spax_grid = np.arange(nspax) * spax_size.value / 1e3
spax_grid = spax_grid - (np.max(spax_grid)/2.)# + (spax_size.value / 1e3 /2.) 


# Create a 2D histogram to identify the particle density
# per spaxel and find which spaxels have zero particles
sperspax, yedges, xedges = np.histogram2d(posy_ang, posx_ang, bins=[spax_grid, spax_grid])

# now bin the x and y angular positions onto this grid
xspax = np.digitize(posx_ang, xedges)
yspax = np.digitize(posy_ang, yedges)

# identify the zero and non-zero spaxels
nonzero = (sperspax != 0)

# Initialise the output wavelength grid and the cube.
# Check here my platform. The laptop can't handle superfine sampling. 
# Not doing this check will CRASH THE LAPTOP.

llims = np.asarray(llims)
lrange = (specs[0].lam >= llims[0]) & (specs[0].lam <= llims[1])

#xwave = specs[0].lam[lrange]
nl = np.size(specs[0].lam[lrange])

t3 = time.time()
# create the large cube that will hold the spectra
outCube = np.zeros((nl, nspax, nspax), dtype=float)
massCube = np.zeros((nspax, nspax), dtype=float)
lwageCube = np.zeros((nspax, nspax), dtype=float)

t4 = time.time()
print 'Cube generated in {0} s' .format(t4-t3)
#fig1 = plt.figure()

# first create a new irregularly spaced wavelength grid, using restwave and z, then a regularly spaced one from that
zlims = llims * (1. + rshift)
zwave = np.linspace(*zlims, num=nl)

# calculate the luminosity distance in pc from cosmology & redshift
dL = cosmo.luminosity_distance(rshift).to(u.pc)

# Now step through the spaxels and find the spectrum

#fig1 = plt.figure()

for j in range(nspax-1):
    for i in range(nspax-1):
            if sperspax[j,i] != 0:
                inSpax = findStars(s, xspax, yspax, i, j)
                massCube[j,i] = np.sum(s['mass'][inSpax])
                # was previously outCube[i,j,:] but swap : and i for FITS writing later
                wave_tmp, spec_temp = genSpaxSpec(s[inSpax], specs, llims, Zvals, Zpos, agevals, vvals)
                
                zspec_temp = np.interp(zwave, wave_tmp*(1.+rshift), spec_temp)
                
                # and now scale the fluxes to redshift; factor 1e6 is to convert from mas-2 to arcsec-2
                outCube[:,j,i] = zspec_temp * (10.*u.pc)**2 / dL**2 / (1.+rshift) / (spax_size*1.E-3)**2           
                

t5 = time.time()
print 'Spaxels loop completed in {0} s' .format(t5-t4)

outHdu = fits.PrimaryHDU(outCube)
h = createHeader(outHdu.header, outCube, spax_grid, zwave)

outHdulist = fits.HDUList([outHdu])
outHdulist.writeto(output_cube,clobber=True)

t7 = time.time()
print 'File written out in {0} s' .format(t7-t5)
